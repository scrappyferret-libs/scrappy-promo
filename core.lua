--- Required libraries.
local json = require( "json" )

--- Localised functions.
local getInfo = system.getInfo
local pathForFile = system.pathForFile
local decodeFile = json.decodeFile
local encode = json.encode
local open = io.open
local close = io.close

-- Localised values.

-- Static values.
local Store = {}
Store.Apple = "apple"
Store.Google = "google"
Store.Amazon = "amazon"
Store.Windows = "windows"
Store.Switch = "nx64"
Store.Steam = "steam"
Store.Itch = "itch"
Store.None = "none"

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	self._configFilename = "scrappyPromo.config"
	self._dataFilename = "scrappy.scrappyPromo"

	self._data = decodeFile( pathForFile( self._dataFilename, system.DocumentsDirectory ) ) or {}

	self:setID( self._params.id )
	self:setURL( self._params.url )

	if self._params.disabled then
		self:disable()
	end

	if self:getURL() and self:getID() then
		self:refreshConfig()
	end

	self:setStore( getInfo( "targetAppStore" ) or Store.None )

	if getInfo( "platform" ) == Store.Switch then
		self:setStore( Store.Switch )
	elseif getInfo( "platform" ) == "macos" or getInfo( "platform" ) == "win32" or getInfo( "platform" ) == "linux" then

		-- Get the dir seperator for this platform
		local dirSeperator = package.config:sub( 1, 1 )

		-- Get the root path
		local path = pathForFile()

		--- Removes the last directory from a path.
		-- @param path The current path.
		-- @return The new path.
		local moveUpOneDir = function( path )

			-- Remove everything after the last forward or back slash so and return the result
			return path:gsub( "(.*)%" .. dirSeperator .. ".*$", "%1" )

		end

		-- Are we on OSX?
		if getInfo( "platform" ) == "macos" then

			-- Remove 'Corona' from the path
			path = moveUpOneDir( path )

			-- Remove 'Resources' from the path
			path = moveUpOneDir( path )

			-- Append the Plugins folder to the path
			path = path .. dirSeperator .. "Plugins"

			-- Append the steam api dyliib file to the end of the path
			path = path .. dirSeperator .. "libsteam_api.dylib"

		-- Are we on Windows?
		elseif getInfo( "platform" ) == "win32" then

			-- Remove 'Resources' from the path
			path = moveUpOneDir( path )

			-- Append the steam api dll file to the end of the path
			path = path .. dirSeperator .. "steam_api.dll"

		-- Are we on Linux? Is anybody?
		elseif getInfo( "platform" ) == "linux" then

		-- Otherwise we're on a non-desktop device
		else

			-- So just quit early and return false
			return false

		end

		-- Try to open the file
		local file = open( path, "r" )

		-- Flag to state whether the file exists or not
		local exists = false

		-- Does it exist?
		if file then

			-- Then flag it
			exists = true

			-- And close out the file handler
			close( file )

			-- And nil it
			file = nil

		end

		-- Are we on Steam?
		if exists then
			self:setStore( Store.Steam )
		else -- FOR NOW JUST ASSUME ITCH
			self:setStore( Store.Itch )
		end

	end

end

function library:refreshConfig()

	if system.getInfo( "platform" ) ~= "nx64" then
			
		local networkListener = function( event )
	    	if event.isError then
	        	print( "Network error - download failed: ", event.response )
	    	elseif event.phase == "ended" then
	
				local filename = pathForFile( self._configFilename, system.DocumentsDirectory )
				local decoded, pos, msg = decodeFile( filename )
	
				if not decoded then
			    	print( "Decode failed at "..tostring(pos)..": "..tostring(msg) )
				else
					self._initiated = true
					self._config = decoded
				end
	
	    	end
		end
	
		local params = {}
	
		network.download(
	    	self:getURL() .. "/config.json",
	    	"GET",
	    	networkListener,
	    	params,
	    	self._configFilename,
	    	system.DocumentsDirectory
		)
		
	end

end

function library:show( params, instant )
	
	if self:isInitiated() and not self._currentPromo and not self._currentPromoConfig and self:isEnabled() then

		self._currentPromo = display.newGroup()

		-- Create a fake back so that the whole screen is touchable to close it
		local back = display.newRect( self._currentPromo, display.contentCenterX, display.contentCenterY, display.contentWidth * 2, display.contentHeight * 2 )
		back.alpha = 0.01

		local onTouch = function( event )
			if event.phase == "began" then
				display.getCurrentStage():setFocus( event.target, event.id )
				event.target.isFocus = true
			elseif event.target.isFocus then
				if event.phase ~= "moved" then
					display.getCurrentStage():setFocus( nil, event.id )
					event.target.isFocus = false
					if self._currentPromoConfig then
						system.openURL( self._currentPromoConfig.stores[ self:getStore() or "generic" ] or "" )
					end
					self:hide()
				end
			end
			return true
		end
		self._currentPromo:addEventListener( "touch", onTouch )

		for k, v in pairs( self._config.promotions ) do

			if not self._data[ k ] and v.ids[ self:getID() ] then

				self._currentPromoConfig = v

				local function networkListener( event )
				    if ( event.isError ) then
				        print ( "Network error - download failed" )
				    else

						if event.target then

							self._data[ k ] = true

							-- SAVE DATA
							-- Path for the file to write
							local path = pathForFile( self._dataFilename, system.DocumentsDirectory )

							-- Open the file handle
							local file, errorString = open( path, "w" )

							if not file then
							    -- Error occurred; output the cause
							    print( "File error: " .. errorString )
							else
							    -- Write data to file
							    file:write( encode( self._data ) )

							    -- Close the file handle
							    close( file )

							end

							file = nil



							if v.banner then

							else


								self._currentPromo:insert( event.target )

								event.target.xScale, event.target.yScale = self:_calculateScaleToFit( event.target, display.contentWidth * ( v.scale or 0.75 ), display.contentHeight * ( v.scale or 0.75 ) )
								event.target.x = display.contentCenterX
								event.target.y = display.contentCenterY

							end

							local onCloseTouch = function( event )
								if event.phase == "began" then
									display.getCurrentStage():setFocus( event.target, event.id )
									event.target.isFocus = true
								elseif event.target.isFocus then
									if event.phase ~= "moved" then
										display.getCurrentStage():setFocus( nil, event.id )
										event.target.isFocus = false
										self:hide()
									end
								end
								return true
							end

							local close = display.newCircle( self._currentPromo, 0, 0, event.target.contentWidth * 0.03 )
							close.x = ( event.target.x + event.target.contentWidth * 0.5 ) - close.contentWidth * 0.2
							close.y = ( event.target.y - event.target.contentHeight * 0.5 ) + close.contentHeight * 0.2
							close:setFillColor( 0, 0, 0, 0.75 )
							close:setStrokeColor( 1, 1, 1, 0.75 )
							close.strokeWidth = 2

							close:addEventListener( "touch", onCloseTouch )

							local options =
							{
							    text = "X",
							    x = close.x,
							    y = close.y,
							    font = native.systemFontBold,
							    fontSize = close.contentHeight * 2
							}

							local x = display.newText( options )
							x:setFillColor( 1, 1, 1, 0.75 )
	 						self._currentPromo:insert( x )
							x:scale( 0.35, 0.35 )

							if not instant then

								if self._transition then
									transition.cancel( self._transition )
									self._transition = nil
								end

								local params = params or { time = 500, alpha = 0.01 }

								self._transition = transition.from( self._currentPromo, params )

							end

						end

				    end

				end

				display.loadRemoteImage( self:getURL() .. "/" .. v.image, "GET", networkListener, v.image, system.TemporaryDirectory, v.width, v.height )

				print( "show",k)
				break

			end

		end

	end

	if not self._currentPromoConfig then
		self:hide( nil, true )
	end

end

function library:hide( params, instant )

	if self._currentPromo then


		local onComplete = function()
			self._currentPromoConfig = nil

			display.remove( self._currentPromo )
			self._currentPromo = nil
		end

		if instant then
			onComplete()
		else

			if self._transition then
				transition.cancel( self._transition )
				self._transition = nil
			end

			local params = params or { time = 250, alpha = 0.01 }
			params.onComplete = onComplete

			self._transition = transition.to( self._currentPromo, params )

		end

	end

end

--- Disables this app.
function library:disable()
	self._disabled = true
	self:hide( nil, true )
end

--- Enables this app.
function library:enable()
	self._disabled = false
end

--- Checks if we're enabled or not.
-- @return True if we're enabled, false otherwise.
function library:isEnabled()
	return not self._disabled
end

--- Checks if we're initiated or not.
-- @return True if we're initiated, false otherwise.
function library:isInitiated()
	return self._initiated
end

--- Gets the ID of this app.
-- @return The id.
function library:getID()
	return self._id
end

--- Sets the ID of this app.
-- @param id The ID.
function library:setID( id )
	self._id = id
end

--- Gets the platform of this app.
-- @return The platform name.
function library:getStore()
	return self._store
end

--- Sets the platform of this app.
-- @param platform The platform name.
function library:setStore( store )
	self._store = store
end

--- Gets the URL of this app.
-- @return The URL.
function library:getURL()
	return self._url
end

--- Sets the URL of this app.
-- @param url The URL.
function library:setURL( url )
	self._url = url
end

function library:update( dt )
	if self._currentPromo then
		self._currentPromo:toFront()
	end
end

--- Calculates the new x and y scale for a display object so that it fits certain bounds.
-- @param object The display object to use for the scale.
-- @param width The width to fit. Optional, if left out then it will use just the height value and keep the aspect ratio the same.
-- @param width The height to fit. Optional, if left out then it will use just the width value and keep the aspect ratio the same.
-- @returns The new x and y scales.
function library:_calculateScaleToFit( object, width, height )

	local xScale, yScale = 1, 1

	if object then

		local newWidth, newHeight

		if width then
			xScale = width / ( object.width or object.contentWidth )
		end

		if height then
			yScale = height / ( object.height or object.contentHeight )
		end

	end

	return xScale or yScale, yScale or xScale

end

--- Destroys this library object.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Promo library
if not Scrappy.Promo then

	-- Then store the library out
	Scrappy.Promo = library

end

-- Return the new library
return library
